CREATE TABLE jeux (
    `id` bigint(20) UNSIGNED NOT NULL,
    `nom` VARCHAR(255) NOT NULL,
    `annee_sortie` VARCHAR(4) NOT NULL,
    `age_min` VARCHAR(3) NOT NULL,
    `min_max_joueur` VARCHAR(255) NOT NULL,
    `min_max_duree` VARCHAR(255),
    description VARCHAR(1000)
);