<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(JeuxTableSeeder::class);
         $this->call(CommentaireTableSeeder::class);
         $this->call(TagsTableSeeder::class);
         $this->call(Tags_JeuxTableSeeder::class);

    }
}
