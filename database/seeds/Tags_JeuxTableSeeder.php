<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Tags_JeuxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('jeux_tag')->insert([
            'tag_id' => 1,
            'jeux_id' => 1
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 3,
            'jeux_id' => 1
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 5,
            'jeux_id' => 1
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 6,
            'jeux_id' => 1
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 7,
            'jeux_id' => 1
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 8,
            'jeux_id' => 1
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 1,
            'jeux_id' => 2
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 11,
            'jeux_id' => 2
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 12,
            'jeux_id' => 2
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 12,
            'jeux_id' => 3
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 1,
            'jeux_id' => 3
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 3,
            'jeux_id' => 3
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 4,
            'jeux_id' => 3
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 5,
            'jeux_id' => 3
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 9,
            'jeux_id' => 3
        ]);
        DB::table('jeux_tag')->insert([
            'tag_id' => 10,
            'jeux_id' => 3
        ]);
    }
}
