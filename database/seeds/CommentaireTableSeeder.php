<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CommentaireTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('commentaires_jeux')->insert([
            'titre' => 'Bon Jeu',
            'body' => 'On retrouve beaucoup de personnages de la série originelle. Très bon jeu en général. Le seul prombème est que ce jeu n\'est disponible seulement sur console! ',
            'auteur'=>'JeanGamer62',
            'jeux_id'=>1,
            'created_at'=>now()
        ]);
    }
}
