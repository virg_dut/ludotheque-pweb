<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('tags')->insert([
            'label' => 'action'
        ]);
        DB::table('tags')->insert([

            'label' => 'hacking'
        ]);
        DB::table('tags')->insert([

            'label' => 'enfant'
        ]);
        DB::table('tags')->insert([

            'label' => 'combat'
        ]);
        DB::table('tags')->insert([

            'label' => 'plate-forme'
        ]);
        DB::table('tags')->insert([

            'label' => 'animation'
        ]);
        DB::table('tags')->insert([

            'label' => 'comics'
        ]);
        DB::table('tags')->insert([

            'label' => 'série TV'
        ]);
        DB::table('tags')->insert([

            'label' => 'Construction'
        ]);
        DB::table('tags')->insert([

            'label' => 'MiniJeux'
        ]);
        DB::table('tags')->insert([

            'label' => 'Infiltration'
        ]);
        DB::table('tags')->insert([

            'label' => 'Aventure'
        ]);

    }
}
