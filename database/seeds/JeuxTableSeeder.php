<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JeuxTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // id	nom	annee_sortie	age_min	min_max_joueur	min_max_duree	urlimg	description
        DB::table('jeux')->insert([
                'nom' => "Les Simpson, Le jeu",
                'annee_sortie' => 2007,
                'age_min' => 3,
                'min_max_joueur' => "1 à 2",
                'min_max_duree' => "20h",
                'urlimg' => "https://i2.cdscdn.com/pdt2/4/2/1/1/300x300/5030931059421/rw/les-simpson-le-jeu-ps3.jpg",
                'description' => "Le jeu présente une histoire inédite rédigée par trois des scénaristes de la série : Tim Long, Matt Selman et Matt Warburton. Dans cette histoire, la famille Simpson découvre sa participation à un nouveau jeu vidéo des Simpson."
        ]);
        DB::table('jeux')->insert([
            'nom' => "Watch Dogs 2",
            'annee_sortie' => 2016,
            'age_min' => 18,
            'min_max_joueur' => "1 à 2",
            'min_max_duree' => "20h",
            'urlimg' => "http://image.jeuxvideo.com/medias-sm/146540/1465404503-8699-jaquette-avant.jpg",
            'description' => "Après Chicago dans le premier opus, San Francisco devient la seconde ville à installer le système de surveillance ctOS (acronyme de l'anglais « city » (ct, signifiant ville), suivi de « Operating System » (OS, signifiant système d'exploitation)), qui centralise toutes les données des habitants et des différents systèmes électroniques de la ville."
        ]);
        //
        DB::table('jeux')->insert([
            'nom' => "Minecraft",
            'annee_sortie' => 2009,
            'age_min' => 3,
            'min_max_joueur' => "1+",
            'min_max_duree' => "Infini",
            'urlimg' => "https://www.minecraft.net/content/dam/archive/og-image/index-hero-og.jpg",
            'description' => "Minecraft est un jeu qui consiste à placer des blocs et à partir dans des aventures."
        ]);
    }
}
