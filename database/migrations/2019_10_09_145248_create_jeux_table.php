<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJeuxTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jeux', function (Blueprint $table) {
            $table->bigIncrements('id')->nullable(false);
            $table->string('nom')->nullable(false);
            $table->string('annee_sortie',4)->nullable(false);
            $table->integer('age_min')->nullable(false);
            $table->string('min_max_joueur')->nullable(false);
            $table->string('min_max_duree');
            $table->string('urlimg');
            $table->string('description');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jeux');
    }
}
