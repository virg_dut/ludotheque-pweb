<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/


//Route::get('/jeux', 'ListeJeux@index')->name('jeux');
Route::resource('jeux', 'ListeJeux');
Route::resource('admin','Admin');

Route::get('apropos','HomeController@about');
Route::get('about','HomeController@about');
Route::get('contact','HomeController@contact');
Route::get('/','HomeController@index');
Route::get('/login','HomeController@login');
Route::resource('comm', 'CommentaireController');
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();



//Route::get('/home', 'HomeController@index')->name('home');
