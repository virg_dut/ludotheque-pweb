<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jeux extends Model
{
    //
    protected $table = 'jeux';
    public $timestamps = false;

    function tags() {
        return $this->belongsToMany(Tag::class);
    }

    function commentaires() {
        return $this->hasMany(Commentaire::class);
    }

}
