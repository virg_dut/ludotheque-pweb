<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    //
    protected $table = 'commentaires_jeux';
    function jeu() {
        return $this->belongsTo(Jeux::class);
    }
}
