<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    //
    protected $table = 'tags';

    function jeu() {
        return $this->belongsTo(Jeux::class);
    }
}
