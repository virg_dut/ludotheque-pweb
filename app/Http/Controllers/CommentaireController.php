<?php

namespace App\Http\Controllers;

use App\Models\Commentaire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentaireController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        echo "coucou";
        $comms = Commentaire::all();
        dd($comms);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate(
            $request,
            [
                'comm' => 'required',
                'aut' => 'required',
                'idjeu' => 'required',
                'aut' => 'required',
                'title' => 'required'
            ]
        );

        // code exécuté uniquement si les données sont validaées
        // sinon un message d'erreur est renvoyé vers l'utilisateur

        // préparation de l'enregistrement à stocker dans la base de données
        $comm = new Commentaire();

        $comm->titre = $request->title;
        $comm->body = $request->comm;
        $comm->auteur = $request->aut;
        $comm->jeux_id = $request->idjeu;




        $comm->save();
        // redirection vers la page qui affiche la liste des tâches
        return redirect('/jeux/'.$request->idjeu);



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commentaire  $commentaire
     * @return \Illuminate\Http\Response
     */
    public function show(Commentaire $commentaire)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commentaire  $commentaire
     * @return \Illuminate\Http\Response
     */
    public function edit(Commentaire $commentaire)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commentaire  $commentaire
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate(
            $request,
            [
                'comm' => 'required',
                'aut' => 'required',
                'idjeu' => 'required',
                'aut' => 'required',
                'title' => 'required'
            ]
        );
        $comm = Commentaire::find($id);
        $comm->titre = $request->title;
        $comm->body = $request->comm;
        $comm->auteur = $request->aut;
        $comm->jeux_id = $request->idjeu;
        $comm->save();

        return redirect()->route("jeux.show", $request->idjeu);




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commentaire  $commentaire
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $req, $idC)
    {
        $comm = Commentaire::find($idC);

       /* if ($req->delete == 'valide') {
            $comm = Commentaire::find($idC);
            $comm->delete();
        }
        return redirect()->route('jeux.index');
        */
       if (Auth::check() && Auth::user()->name == $comm->auteur && $req->delete == 'valid') {
           $idjeu = $comm->jeux_id;
            $comm ->delete();
            return redirect()->route("jeux.show",$idjeu);
       } else {
           return view("error.403");
       }

       }
}
