<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jeux;

class Admin extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //

        $jeux = Jeux::all();


        return view('jeux.admin', ['jeux' => $jeux]);


    }


}
