<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jeux;

class ListeJeux extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
/*    public function __construct() {
        $this->middleware('auth');
    }
*/


    public function index()
    {
        //
        $jeux = Jeux::all();


        return view ('jeux.index',['jeux' => $jeux]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('jeux.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(
            $request,
            [
                'nom' => 'required',
                'age_min' => 'required',
                'description' => 'required',
            ]
        );

        // code exécuté uniquement si les données sont validaées
        // sinon un message d'erreur est renvoyé vers l'utilisateur

        // préparation de l'enregistrement à stocker dans la base de données
        $jeux = new Jeux;

        $jeux->nom = $request->nom;
        $jeux->age_min = $request->age_min;
        $jeux->annee_sortie = $request->annee_sortie;
        $jeux->description = $request->description;
        $jeux->urlimg = $request->urlimg;
        $jeux->min_max_joueur = $request->min_max_joueur;
        $jeux->min_max_duree = $request->min_max_duree;

        $jeux->save();

        // redirection vers la page qui affiche la liste des tâches
        return redirect('/jeux');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        // ici des trucs interessants !
        $action = $request->query('action', 'show');
        $jeux = Jeux::find($id);
        $comm = $jeux->commentaires;
        $tags = $jeux->tags;
        return view('jeux.show', ['jeux' => $jeux, 'action' => $action, 'comm'=>$comm,'tags'=>$tags]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $jeux = Jeux::find($id);
        return view('jeux.edit', ['jeux' => $jeux]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $jeux = Jeux::find($id);

        $this->validate(
            $request,
            [
                'nom' => 'required',
                'age_min' => 'required',
                'description' => 'required',
            ]
        );
        $jeux->nom = $request->nom;
        $jeux->age_min = $request->age_min;
        $jeux->annee_sortie = $request->annee_sortie;
        $jeux->description = $request->description;
        $jeux->urlimg = $request->urlimg;
        $jeux->min_max_joueur = $request->min_max_joueur;
        $jeux->min_max_duree = $request->min_max_duree;
        $jeux->save();

        return redirect('/jeux');




    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if ($request->delete == 'valide') {
            $jeux = Jeux::find($id);
            $jeux->delete();
        }
        return redirect()->route('jeux.index');
    }
}
