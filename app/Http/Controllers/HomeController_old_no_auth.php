<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view ('welcome');
    }



    public function about()
    {
        //
        return view ('about');
    }

    public function contact()
    {
        //
        return view ('contact');
    }




}
