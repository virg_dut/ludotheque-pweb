# Projet Ludotheque PWEB 2019

## PAS ENCORE TERMINE (mq juste edition/suppr commentaires) AU 31/12 ... EH OUI, je prends mon temps



## Éléments Requis
- php7.3 (minimum) + extensions pdo (mysql) / xml / zip
- composer
- postgres

**Je conseille vivement l'utilisation de postgres** mysql n'a pas été testé !



## Comment utiliser le projet ?

- `composer install`
- Configurer le .env
- `php artisan migrate:fresh`
- `php artisan db:seed`
- `php artisan serve`

## Pour avoir une édition compléte ...

- Créer un compte en cliquant sur 'Invité' en haut de l'écran
- Créer un compte avec nom d'utilisateur et mot de passe
- On est automatiquement connecté après s'être inscrit

## Pour avoir accès aux crud des jeux

- En étant connecté, cliquer sur 'Administration' dans la barre de navigation
- On retrouve un pannel avec tout les jeux présents dans la BDD
    - 'Infos' => Affiche le jeu
    - 'Éditer' => permet d'éditer le jeu
    - 'Supprimer' => permet d'effacer le jeu en affichant ses informations (validation de la suppression)

## Ecrire un commentaire

- En étant connecté, cliquer dans la liste des jeux sur 'Infos'
- En bas de la page on retrouve un menu permettant d'écrire un commentaire (celui ci n'est pas présent quand l'utilisateur n'est pas connecté)
- Appuyer sur Valider confirme l'envoi du commentaire puis renvoie sur la page du jeu

## Se déconnecter
- Cliquer sur le nom d'utilisateur en haut à droite

## Se connecter
- Cliquer sur le 'Invité' en haut à droite


