@extends('layouts.header')
@section('content')
    <div class="container">
        <h1 style="font-weight: bold; color:red">Non, il n'est pas possible de me contacter!</h1>
        <p>Par contre, si vous aimez de la musique rock, vous pouvez écouter Grand Lille Info</p>
        <audio controls autoplay src="http://str0.creacast.com/grandlilleinfos">
            Dommage, votre navigateur ne supporte pas la lecture automatique ...
        </audio>
    </div>
@endsection
