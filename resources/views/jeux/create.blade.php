{{--
   messages d'erreurs dans la saisie du formulaire.
--}}
@extends('layouts.header')
@section("content")
<div class="container">

@if ($errors->any())
    <div>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
{{--
     formulaire de saisie d'une tâche
     la fonction 'route' utilise un nom de route
     'csrf_field' ajoute un champ caché qui permet de vérifier
       que le formulaire vient du serveur.
  --}}

<form action="{{route('jeux.store')}}" method="POST">
    {!! csrf_field() !!}
    <div class="text-center" style="margin-top: 2rem">
        <h3>Ajout d'un jeu</h3>
        <hr class="mt-2 mb-2">
    </div>
    <div>
        {{-- la date d'expiration  --}}
        <label for="nj"><strong>Nom du jeu : </strong></label>
        <input type="text" name="nom" id="nj"
               value="{{ old('nom') }}"
               placeholder="Nom du Jeu">
    </div>
    <div>
        {{-- la catégorie  --}}
        <label for="as"><strong>Année de Sortie</strong></label>
        <input type="text" class="form-control" id="as" name="annee_sortie"
               value="{{ old('annee_sortie') }}">
    </div>
    <div>
        {{-- la catégorie  --}}
        <label for="am"><strong>Age minimal</strong></label>
        <input type="text" class="form-control" id="am" name="age_min"
               value="{{ old('age_min') }}">
    </div>
    <div>
        {{-- la catégorie  --}}
        <label for="mmj"><strong>Joueur Min / Max</strong></label>
        <input type="text" class="form-control" id="mmj" name="min_max_joueur"
               value="{{ old('min_max_joueur') }}">
    </div>
    <div>
        {{-- la catégorie  --}}
        <label for="mmd"><strong>Durée Min / Max</strong></label>
        <input type="text" class="form-control" id="mmd" name="min_max_duree"
               value="{{ old('min_max_duree') }}">
    </div>
    <div>
        {{-- la catégorie  --}}
        <label for="iu"><strong>Url Image</strong></label>
        <input type="text" class="form-control" id="iu" name="urlimg"
               value="{{ old('urlimg') }}">
    </div>
    <div>
        <label for="textarea-input"><strong>Description :</strong></label>
        <textarea name="description" id="description" rows="6" class="form-control"
                  placeholder="Description..">{{ old('description') }}</textarea>
    </div>
    <div>
        <button class="btn btn-success" type="submit">Valide</button>
    </div>
</form>
</div>
    @endsection
