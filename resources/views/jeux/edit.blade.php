{{--
   messages d'erreurs dans la saisie du formulaire.
--}}
@extends('layouts.header')
@section("content")
<div class="container">
    {{--
       messages d'erreurs dans la saisie du formulaire.
    --}}


    @if ($errors->any())
        <div>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--
         formulaire de saisie d'une tâche
         la fonction 'route' utilise un nom de route.
         '@csrf' ajoute un champ caché qui permet de vérifier
           que le formulaire vient du serveur.
         '@method('PUT') précise à Laravel que la requête doit être traitée
          avec une commande PUT du protocole HTTP.
      --}}
    <form action="{{route('jeux.update',$jeux->id)}}" method="POST">
        @csrf
        @method('PUT')
       <div class="text-center" style="margin-top: 2rem">
            <h3>Modification d'un jeu</h3>
            <hr class="mt-2 mb-2">
        </div>
        <div>
            {{-- la date d'expiration  --}}
            <label for="nj"><strong>Nom du jeu : </strong></label>
            <input type="text" name="nom" id="nj"
                   value="{{  $jeux->nom }}"
                   placeholder="Nom du Jeu">
        </div>
        <div>
            {{-- la catégorie  --}}
            <label for="as"><strong>Année de Sortie</strong></label>
            <input type="text" class="form-control" id="as" name="annee_sortie"
                   value="{{ $jeux->annee_sortie }}">
        </div>
        <div>
            {{-- la catégorie  --}}
            <label for="am"><strong>Age minimal</strong></label>
            <input type="text" class="form-control" id="am" name="age_min"
                   value="{{ $jeux->age_min }}">
        </div>
        <div>
            {{-- la catégorie  --}}
            <label for="mmj"><strong>Joueur Min / Max</strong></label>
            <input type="text" class="form-control" id="mmj" name="min_max_joueur"
                   value="{{ $jeux->min_max_joueur }}">
        </div>
        <div>
            {{-- la catégorie  --}}
            <label for="mmd"><strong>Durée Min / Max</strong></label>
            <input type="text" class="form-control" id="mmd" name="min_max_duree"
                   value="{{ $jeux->min_max_duree }}">
        </div>
        <div>
            {{-- la catégorie  --}}
            <label for="iu"><strong>Url Image</strong></label>
            <input type="text" class="form-control" id="iu" name="urlimg"
                   value="{{ $jeux->urlimg }}">
        </div>
        <div>
            <label for="textarea-input"><strong>Description :</strong></label>
            <textarea name="description" id="description" rows="6" class="form-control"
                      placeholder="Description..">{{ $jeux->description }}</textarea>
        </div>
        <div>
            <button class="btn btn-success" type="submit">Valide</button>
        </div>
    </form>
</div>
@endsection
