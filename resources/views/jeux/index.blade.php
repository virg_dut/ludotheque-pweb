@extends('layouts.header')
@section('content')


<div class="container">
    <h1>Liste des jeux</h1>
    @if(!empty($jeux))
    <br>
    <div class="row">
        @foreach($jeux as $jeu)
            @if($jeu['id'] % 2 == 1 && $jeu['id'] != 1)
                </div>
                <br>
                <div class="row">
            @endif
                <div class="col">
                    <div class="card" style="width:400px">
                        <img class="card-img-top" src="{{$jeu['urlimg']}}"  style="height:100px;object-fit: cover;" alt="Card image">
                        <div class="card-body">
                            <h4 class="card-title">{{$jeu['nom']}}</h4>
                            <h5>Année Sortie : {{$jeu['annee_sortie']}}</h5>
                            <h5>Age Mini : {{$jeu['age_min']}} ans</h5>
                            <h5>Nombre joueurs : {{$jeu['min_max_joueur']}}</h5>
                            <h5>Durée : {{$jeu['min_max_duree']}}</h5>
                            <p class="card-text">{{$jeu['description']}}</p>
                            <a class="btn btn-secondary" href="{{route('jeux.show',$jeu['id'])}}">Infos</a>
                        </div>
                    </div>
                </div>
        @endforeach
                </div>
    @else
        <h2>Aucun jeu d'entré !</h2>
    @endif
</div>
@endsection
