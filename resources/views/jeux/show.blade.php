<?php


?>
@extends('layouts.header')
@section("content")

<div class="container">
    <br>
    <div class="row">
        <div class="col-sm-4"></div>
        <div class="col-sm-4">
            <div class="card" style="width:400px">
                <img class="card-img-top" src="{{$jeux->urlimg}}"  style="height:100px;object-fit: cover;" alt="Card image">
                <div class="card-body">
                    <h4 class="card-title">{{$jeux->nom}}</h4>
                    <h5>Année Sortie : {{$jeux->annee_sortie}}</h5>
                    <h5>Age Mini : {{$jeux->age_min}} ans</h5>
                    <h5>Nombre joueurs : {{$jeux->min_max_joueur}}</h5>
                    <h5>Durée : {{$jeux->min_max_duree}}</h5>
                    <p class="card-text">{{$jeux->description}}</p>
                    @if($action == 'delete')
                        <form action="{{route('jeux.destroy',$jeux->id)}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <div class="text-center">
                                <button type="submit" name="delete" value="valide">Valider</button>
                                <button type="submit" name="delete" value="annule">Annuler</button>
                            </div>
                        </form>
                    @else
                        <div>
                            <a href="{{route('jeux.index')}}">Retour à la liste</a>
                        </div>
                    @endif


                </div>
            </div>
        </div>

    </div>
    <br>
    @if ($action != 'delete')
    <h4>Tags</h4>
        <div style="text-align: center;">
        @foreach($tags as $tag)

            <span class="badge badge-info">{{$tag['label']}}</span>

        @endforeach
        </div>
            <br>



        <h4>Commentaires</h4>
    <div class="row">

        <div class="col-sm-2"></div>
        <div class="col-sm-8">
    @if( sizeof($comm)>0)
        @foreach($comm as $commentaire)
                    <div class="card text-white bg-secondary mb-3" style="width:100%;">
                            <div class="card-header"><b>{{$commentaire['titre']}}</b> par {{$commentaire['auteur']}} le {{$commentaire['created_at']}}</div>
                            <div class="card-body">
                                {{$commentaire['body']}}
                                @if(Auth::check() && Auth::user()->name == $commentaire['auteur'] )
                                        <br><br>
                                    <a href="#" data-toggle="modal" data-target="#modEditCom{{ $commentaire['id'] }}">Éditer</a> <a href="#" data-toggle="modal" data-target="#modSuppCom{{ $commentaire['id'] }}">Supprimer</a>

                                    <!-- Modale d'édition -->
                                    <!-- Modal -->
                                    <div class="modal fade" id="modEditCom{{ $commentaire['id'] }}" tabindex="-1" role="dialog" aria-labelledby="Editer" aria-hidden="true" style="color:black;">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Éditer le commentaire</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <form method="post" action="{{ route("comm.update", $commentaire["id"]) }}">
                                                    @method('PUT')
                                                <div class="modal-body">
                                                    {!! csrf_field() !!}
                                                    <label for="titcom">Titre du commentaire</label><br>
                                                    <input type="text" id="titcom" name="title" value="{{$commentaire['titre']}}" required><br>
                                                    <label for="comment">Commentaire</label>
                                                    <textarea class="form-control" rows="5" id="comment" name="comm" required>{{ $commentaire['body'] }}</textarea>
                                                    <input type="hidden" name="idjeu" value="{{$jeux->id}}">
                                                    <input type="hidden" name="aut" value="{{Auth::user()->name}}">
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" >Fermer</button>
                                                    <button type="submit" class="btn btn-primary">Confirmer</button>
                                                </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>


                                    <!-- Modale de suppression -->
                                    <div class="modal fade" id="modSuppCom{{ $commentaire['id'] }}" style="color:black">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title">Supprimer le commentaire</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <p>Êtes-vous sûr de supprimer le commentaire ?</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <form method="post" action="{{ route("comm.destroy", $commentaire['id']) }}">
                                                        @csrf
                                                        @method('DELETE')
                                                        <button type="submit" type="button" name="delete" value="valid" class="btn btn-danger">Oui</button>
                                                    </form>

                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>












                                @endif
                            </div>



                    </div>

        @endforeach
    @else
                <h5>Pas de commentaire</h5>
    @endif
            <hr>
        @if(Auth::check())
            <!--  -->
            <h4>Votre commentaire !</h4>
            <form action="{{route('comm.store')}}" method="POST">
                <div class="form-group">
                    {!! csrf_field() !!}
                    <label for="titcom">Titre du commentaire</label><br>
                    <input type="text" id="titcom" name="title" placeholder="Le titre de votre commentaire" required><br>
                    <label for="comment">Commentez !</label>
                    <textarea class="form-control" rows="5" id="comment" placeholder="Votre Commentaire ici" name="comm" required></textarea>
                    <input type="hidden" name="idjeu" value="{{$jeux->id}}">
                    <input type="hidden" name="aut" value="{{Auth::user()->name}}">
                    <input type="submit" value="Envoyer">
                </div>
            </form>
        @endif
        </div>
    </div>
    @endif
</div>
@endsection
