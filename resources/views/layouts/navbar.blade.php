
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="#">Ludothèque</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Menu">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarColor02">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">Accueil </a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/jeux">Jeux<span class="sr-only">(actif)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/contact">Contact</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/about">A Propos</a>
            </li>
            @if(Auth::check())
                <li class="nav-item">
                    <a class="nav-link" href="/admin">Administration</a>
                </li>
            @endif

        </ul>
        <!--<form class="form-inline my-2 my-lg-0">
            <input class="form-control mr-sm-2" type="text" placeholder="Recherche">
            <button class="btn btn-secondary my-2 my-sm-0" type="submit">OK</button>
        </form>-->
        <div class="form-inline my-2 my-lg-0">
            @if (Auth::check())
                <a class="btn btn-secondary my-2 my-sm-0" href="/logout">{{Auth::user()->name}}</a>
                @else
                <a class="btn btn-secondary my-2 my-sm-0" href="/login">Invité</a>
                @endif</a>
        </div>

    </div>
</nav>

