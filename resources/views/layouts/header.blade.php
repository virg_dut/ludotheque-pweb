<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Jeux</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <style>
        body {
            height: 100%;
        }
        #page-content {
            margin-bottom: 10%;
        }
        #sticky-footer {
            position: fixed;
            width: 100%;
            bottom: 0;
        }
    </style>
</head>
<body>
@include('layouts.navbar')
<div id="page-content">
@yield('content')
</div>
@include('layouts.footer')
</body>
</html>
